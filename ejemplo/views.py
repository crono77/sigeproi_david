from django.forms import formset_factory
from django.db import IntegrityError,transaction
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages

from .models import Expositor, Conferencia
from eventos .models import Seccion
from actividades .models import Actividades
from config.settings import NO_REGISTROS_PAGINACION
from config.entorno import *
from .forms import ConferenciaForm
from .forms import ExpositorForm
from programa_de_actividades.models import Programa_actividades



@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_expositor')
def registrar_expositor(request):
    if request.method == 'POST':
        form = ExpositorForm(request.POST)
        if form.is_valid():
            nombre = request.POST['nombre']
            apellido_paterno = request.POST['apellido_paterno']
            apellido_materno = request.POST['apellido_materno']
            biografia = request.POST['biografia']
            email = request.POST['email']
            redes_sociales = request.POST['redes_sociales']
            nacionalidad = request.POST['nacionalidad']

            es_extranjero = request.POST.get('es_extranjero', False)
            if es_extranjero == 'on':
                es_extranjero = True

            empresa = request.POST['empresa']
            puesto_expositor = request.POST['puesto_expositor']

            registrar_expositor= Expositor(nombre=nombre, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, biografia=biografia,
                                           email=email, redes_sociales=redes_sociales, nacionalidad=nacionalidad, es_extranjero=es_extranjero, empresa=empresa,
                                           puesto_expositor=puesto_expositor)
            registrar_expositor.save()
            messages.add_message(request, messages.INFO, SMS_REGISTRADO)
            return HttpResponseRedirect('/conferencias/consultar_expositor/')
    else:
        form = ExpositorForm()

    return render(request, 'portal/formulario.html', {'form': form, 'tituloform': 'Registro Expositor'})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_expositor')
def consultar_expositor(request):
    NOMBRE_TABLA = 'Consulta Expositor'
    URL_ALTA = 'conferencias:registrar_expositor'
    URL_VER = 'conferencias:ver_expositor'
    URL_EDITAR = 'conferencias:editar_expositor'
    URL_ELIMINAR = 'conferencias:eliminar_expositor'

    buscar = request.POST.get('buscar')
    page = request.GET.get('page')
    listaConsulta = []

    if buscar:
        ban_busqueda = True
        consulta = Expositor.objects.all().filter(nombre__icontains=buscar) \
                   | Expositor.objects.all().filter(apellido_paterno__icontains=buscar)
    else:
        ban_busqueda = False
        consulta = Expositor.objects.all()

    paginator = Paginator(consulta, NO_REGISTROS_PAGINACION)

    try:
        resultadoConsulta = paginator.page(page)
    except PageNotAnInteger:
        resultadoConsulta = paginator.page(1)
    except EmptyPage:
        resultadoConsulta = paginator.page(paginator.num_pages)


    listaColumnas = ['#','Nombre','Email','Nacionalidad']

    for dato in resultadoConsulta:

        listaConsulta.append([dato.id,
                              dato,
                              dato.email,
                              dato.nacionalidad])


    return render(request, 'portal/consulta.html', {'datos_paginador': resultadoConsulta,
                                                    'columnas':listaColumnas,
                                                    'datos_tabla': listaConsulta,
                                                    'busqueda':ban_busqueda,
                                                    'URL_ALTA':URL_ALTA,
                                                    'URL_VER': URL_VER,
                                                    'URL_EDITAR': URL_EDITAR,
                                                    'URL_ELIMINAR': URL_ELIMINAR,
                                                    'NOMBRE_TABLA':NOMBRE_TABLA})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.change_expositor')
def editar_expositor(request,id):
    NOMBRE_FORMULARIO = "Editar Expositor"
    ExpositorFormSet = formset_factory(ExpositorForm, extra=1, max_num=1)
    if request.method == 'POST':
        formset = ExpositorFormSet(request.POST)
        if formset.has_changed():
            if formset.is_valid():
                nombre= request.POST['form-0-nombre']
                apellido_paterno = request.POST['form-0-apellido_paterno']
                apellido_materno = request.POST['form-0-apellido_materno']
                biografia = request.POST['form-0-biografia']
                email = request.POST['form-0-email']
                redes_sociales = request.POST['form-0-redes_sociales']
                nacionalidad = request.POST['form-0-nacionalidad']
                es_extranjero = request.POST.get('form-0-es_extranjero', False)
                if es_extranjero == 'on':
                    es_extranjero = True
                empresa = request.POST['form-0-empresa']
                puesto_expositor = request.POST['form-0-puesto_expositor']

                actualizar = Expositor.objects.get(id=id)
                actualizar.nombre = nombre
                actualizar.apellido_paterno = apellido_paterno
                actualizar.apellido_materno = apellido_materno
                actualizar.biografia = biografia
                actualizar.email = email
                actualizar.redes_sociales = redes_sociales
                actualizar.nacionalidad = nacionalidad
                actualizar.es_extranjero = es_extranjero
                actualizar.empresa = empresa
                actualizar.puesto_expositor = puesto_expositor
                actualizar.save()

                return HttpResponseRedirect('/conferencias/consultar_expositor/')
    else:

        consulta = Expositor.objects.filter(id=id)

        for dato in consulta:
            formset = ExpositorFormSet(initial=[{'nombre': dato.nombre,
                                            'apellido_paterno': dato.apellido_paterno,
                                            'apellido_materno': dato.apellido_materno,
                                            'redes_sociales': dato.redes_sociales,
                                            'nacionalidad': dato.nacionalidad,
                                            'es_extranjero': dato.es_extranjero,
                                            'biografia': dato.biografia,
                                            'email': dato.email,
                                            'empresa': dato.empresa,
                                            'puesto_expositor': dato.puesto_expositor}])

    messages.add_message(request, messages.INFO, SMS_EDITADO)
    return render(request, 'portal/formulario_editar.html', {'tituloform': NOMBRE_FORMULARIO,
                                                             'form': formset})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_expositor')
def ver_expositor(request, id):
    LISTA = []
    NOMBRE_FORMULARIO = 'Ver Datos del Expositor'
    consulta = Expositor.objects.filter(id=id)

    for dato in consulta:
        if dato.es_extranjero:
            es_extranjero = 'Si'
        else:
            es_extranjero = 'No'

        LISTA.append(['TipoTexto', 'Nombre', dato.nombre+' '+dato.apellido_paterno+' '+dato.apellido_materno])
        LISTA.append(['TipoTexto', 'Biografia', dato.biografia])
        LISTA.append(['TipoTexto', 'Email', dato.email])
        LISTA.append(['TipoTexto', 'Redes sociales', dato.redes_sociales])
        LISTA.append(['TipoTexto', 'Nacionalidad', dato.nacionalidad])
        LISTA.append(['TipoTexto', 'Es extranjero', es_extranjero])
        LISTA.append(['TipoTexto', 'Empresa', dato.empresa])
        LISTA.append(['TipoTexto', 'Puesto del expositor', dato.puesto_expositor])

    return render(request, 'portal/formulario_ver.html', {'tituloform': NOMBRE_FORMULARIO, 'datos': LISTA})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.delete_expositor')
def eliminar_expositor(request, id):
    try:
        eliminar = Expositor.objects.get(id=id)
        eliminar.delete()
        messages.add_message(request, messages.INFO,SMS_ELIMINADO)
    except:
        messages.add_message(request, messages.INFO,SMS_ELIMINADO_ERROR)

    return HttpResponseRedirect('/conferencias/consultar_expositor/')


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_conferencia')

def registrar_conferencia(request):
    if request.method == 'POST':
        form = ConferenciaForm(request.POST)
        if form.is_valid():
            titulo = request.POST['titulo']
            descripcion = request.POST['descripcion']

            publicada = request.POST.get('publicada', False)
            if publicada == 'on':
                publicada = True

            cupo_asistentes = request.POST['cupo_asistentes']
            if request.POST.get('cupo_asistentes') == '':
                cupo_asistentes = None

            expositor = Expositor.objects.get(id=request.POST['expositor'])

            insertar_actividad= Actividades(titulo=titulo, descripcion=descripcion, publicada=publicada, cupo_asistentes=cupo_asistentes,
                                            seccion=Seccion.objects.get(id=1))
            insertar_actividad.save()

            registrar_conferencia = Conferencia(expositor=expositor, actividad=insertar_actividad)


            registrar_conferencia.save()
            messages.add_message(request, messages.INFO, SMS_REGISTRADO)
            return HttpResponseRedirect('/conferencias/consultar_conferencias/')

    else:
        form = ConferenciaForm()

    return render(request, 'portal/formulario.html', {'form': form, 'tituloform': 'Registro Conferencia'})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_conferencia')
def consultar_conferencias(request):
    NOMBRE_TABLA = 'Consulta Conferencias'
    URL_ALTA = 'conferencias:registrar_conferencia'
    URL_VER = 'conferencias:ver_conferencia'
    URL_EDITAR = 'conferencias:editar_conferencia'
    URL_ELIMINAR = 'conferencias:eliminar_conferencia'

    buscar = request.POST.get('buscar')
    page = request.GET.get('page')
    listaConsulta = []

    if buscar:
        ban_busqueda = True
        consulta = Conferencia.objects.all().filter(expositor__nombre__icontains=buscar) \
                   | Conferencia.objects.all().filter(expositor__apellido_paterno__icontains=buscar) \
                   | Conferencia.objects.all().filter(expositor__apellido_materno__icontains=buscar) \
                   | Conferencia.objects.all().filter(actividad__titulo__icontains=buscar)
    else:
        ban_busqueda = False
        consulta = Conferencia.objects.all()

    paginator = Paginator(consulta, NO_REGISTROS_PAGINACION)

    try:
        resultadoConsulta = paginator.page(page)
    except PageNotAnInteger:
        resultadoConsulta = paginator.page(1)
    except EmptyPage:
        resultadoConsulta = paginator.page(paginator.num_pages)


    listaColumanas = ['#','Titulo', 'Expositor', 'Cupo asistentes', 'Publicada']

    for dato in resultadoConsulta:

        if dato.actividad.publicada:
            publicada = 'Si'
        else:
            publicada = 'No'
        listaConsulta.append([dato.id,
                              dato.actividad.titulo,
                              dato.expositor,
                              dato.actividad.cupo_asistentes,
                              publicada])


    return render(request, 'portal/consulta.html', {'datos_paginador': resultadoConsulta,
                                                    'columnas':listaColumanas,
                                                    'datos_tabla': listaConsulta,
                                                    'busqueda':ban_busqueda,
                                                    'URL_ALTA':URL_ALTA,
                                                    'URL_VER': URL_VER,
                                                    'URL_EDITAR': URL_EDITAR,
                                                    'URL_ELIMINAR': URL_ELIMINAR,
                                                    'NOMBRE_TABLA':NOMBRE_TABLA})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.change_conferencia')
def editar_conferencia(request,id):
    NOMBRE_FORMULARIO = "Editar Conferencias"
    ConferenciasFormSet = formset_factory(ConferenciaForm, extra=1, max_num=1)
    if request.method == 'POST':
        formset = ConferenciasFormSet(request.POST)
        if formset.has_changed():
            if formset.is_valid():
                titulo = request.POST['form-0-titulo']
                descripcion = request.POST['form-0-descripcion']

                publicada = request.POST.get('form-0-publicada', False)
                if publicada == 'on':
                    publicada = True

                cupo_asistentes = request.POST['form-0-cupo_asistentes']
                if request.POST.get('form-0-cupo_asistentes') == '':
                    cupo_asistentes = None

                expositor = Expositor.objects.get(id=request.POST['form-0-expositor'])

                actualizar_conferencia = Conferencia.objects.get(id=id)
                actualizar_conferencia.expositor = expositor
                actualizar_conferencia.save()

                actualizar_actividad = Actividades.objects.get(id=actualizar_conferencia.actividad.id)
                actualizar_actividad.titulo = titulo
                actualizar_actividad.descripcion = descripcion
                actualizar_actividad.publicada = publicada
                actualizar_actividad.cupo_asistentes = cupo_asistentes
                actualizar_actividad.save()


                return HttpResponseRedirect('/conferencias/consultar_conferencias/')
    else:

        consulta = Conferencia.objects.filter(id=id)

        for dato in consulta:
            formset = ConferenciasFormSet(initial=[{'expositor': dato.expositor,
                                            'titulo': dato.actividad.titulo,
                                            'descripcion': dato.actividad.descripcion,
                                            'publicada': dato.actividad.publicada,
                                            'cupo_asistentes': dato.actividad.cupo_asistentes,
                                            'fecha_creacion': dato.actividad.fecha_creacion,
                                            'seccion': dato.actividad.seccion}])

    messages.add_message(request, messages.INFO, SMS_EDITADO)
    return render(request, 'portal/formulario_editar.html', {'tituloform': NOMBRE_FORMULARIO,
                                                             'form': formset})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.add_conferencia')
def ver_conferencia(request, id):
    LISTA = []
    NOMBRE_FORMULARIO = 'Ver Datos de la Conferencia'
    consulta = Conferencia.objects.filter(id=id)

    for dato in consulta:
        if dato.actividad.publicada:
            publicada = 'Si'
        else:
             publicada = 'No'

        LISTA.append(['TipoTexto', 'Expositor', dato.expositor])
        LISTA.append(['TipoTexto', 'Titulo', dato.actividad.titulo])
        LISTA.append(['TipoTexto', 'Descripcion', dato.actividad.descripcion])
        LISTA.append(['TipoTexto', 'Publicada', publicada])
        LISTA.append(['TipoTexto', 'Cupo de asistentes', dato.actividad.cupo_asistentes])
        LISTA.append(['TipoTexto', 'Fecha de creacion', dato.actividad.fecha_creacion])
        LISTA.append(['TipoTexto', 'Seccion', dato.actividad.seccion])

    return render(request, 'portal/formulario_ver.html', {'tituloform': NOMBRE_FORMULARIO, 'datos': LISTA})


@login_required(login_url=URL_PORTAL_ADMIN)
@permission_required('conferencias.delete_conferencia')
@transaction.atomic
def eliminar_conferencia(request, id):
    try:
        eliminar = Conferencia.objects.get(id=id)
        eliminar_actividad = Actividades.objects.get(id=eliminar.actividad.id)

        if Programa_actividades.objects.filter(actividad__id=eliminar_actividad.id).count():

            eliminar_programa = Programa_actividades.objects.get(actividad__id=eliminar_actividad.id)
            eliminar_programa.delete()
            eliminar_actividad.delete()
            eliminar.delete()
        else:
            eliminar.delete()
            eliminar_actividad.delete()
        messages.add_message(request, messages.INFO, SMS_ELIMINADO)
    except:
        messages.add_message(request, messages.INFO, SMS_ELIMINADO_ERROR)

    return HttpResponseRedirect('/conferencias/consultar_conferencias/')