from django import forms
from conferencias.models import Expositor


class ExpositorForm(forms.Form):
    nombre = forms.CharField(label='*Nombre', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    apellido_paterno = forms.CharField(label='Apellido paterno', required=False, max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    apellido_materno = forms.CharField(label='*Apellido materno', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    biografia = forms.CharField(label='Biografia', required=False, max_length=1500, widget=forms.Textarea(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput(attrs={'class': 'form-control','type':'email'}))
    redes_sociales = forms.CharField(label='Redes sociales', required=False, max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))
    nacionalidad = forms.CharField(label='Nacionalidad', required=False, max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'}))
    es_extranjero = forms.BooleanField(label='Es extranjero', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    empresa = forms.CharField(label='Empresa', required=False, max_length=100,widget=forms.TextInput(attrs={'class': 'form-control'}))
    puesto_expositor = forms.CharField(label='*Puesto del Expositor', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))


class ConferenciaForm(forms.Form):
    titulo = forms.CharField(label='*Titulo', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(label='Descripcion', required=False, max_length=750, widget=forms.Textarea(attrs={'class': 'form-control'}))
    publicada = forms.BooleanField(label='Publicada', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    cupo_asistentes = forms.IntegerField(label='Cupo asistentes', max_value=99999,required=False, widget=forms.TextInput(attrs={'class': 'form-control','type':'number','value':0}))
    expositor = forms.ModelChoiceField(label='*Expositor', widget=forms.Select(attrs={'class': 'form-control'}), queryset=Expositor.objects.all())
