from django.db import models
from django.contrib.auth.models import User
from catalogos.models import Carrera
from catalogos.models import DependenciaExterna

Usuario_tipo_usuario=[
        (1,'Investigador'),
        (2,'Alumno'),
        (3,'Externo'),
        (4, 'Directivo')
      ] 

class Usuario(models.Model):

    auth_user=models.ForeignKey(User,on_delete=models.PROTECT)
    nombre=models.CharField(max_length=20, null=False,blank=False)
    apellido_paterno=models.CharField(max_length=20, null=False,blank=False)
    apellido_materno=models.CharField(max_length=20, null=False,blank=False)
    tipo_usuario=models.IntegerField(
        null=False, blank=False,
        choices=Usuario_tipo_usuario
        )
    curp=models.CharField(max_length=18,null=False,blank=False)
    rfc=models.CharField(max_length=13,null=False,blank=False)
    telefono=models.CharField(max_length=15,null=False,blank=False)
    foto=models.TextField()
    activo=models.CharField(max_length=1,null=False,blank=False)
    semestre_actual=models.IntegerField()
    carrera = models.ForeignKey(Carrera, on_delete=models.PROTECT, null=False)
    procedencia = models.CharField(max_length=45,null=False,blank=False)
    cargo=models.CharField(max_length=45,null=False,blank=False)

class InvestigadorNumeroExterna(models.Model):
    externo= models.ForeignKey(DependenciaExterna, on_delete=models.PROTECT, null=False)
    investigador = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    numero = models.CharField(max_length=30,null=False,blank=False)

    def __str__(self):
        return "%s - %s - %s" % (self.investigador.nombre,self.externo.nombre_largo,self.numero)

    class Meta(object):
        verbose_name = "Investigador Clave Externa"
        verbose_name_plural = "Investigador Claves Externa"
        ordering = ['investigador']


class CedulaInvestigador(models.Model):
    numero_cedula = models.CharField(max_length=15, null=False, blank=False)
    pais = models.CharField(max_length=20, null=False, blank=False)
    clave_programa = models.CharField(max_length=10, null=False, blank=False)
    nombre_programa = models.CharField(max_length=50, null=False, blank=False)
    clave_institucion = models.CharField(max_length=10, null=False, blank=False)
    nombre_institucion = models.CharField(max_length=50, null=False, blank=False)
    fecha_expedicion = models.DateField(null=False, blank=False)
    investigador = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)

    def __str__(self):
        return "%s - %s - %s" % (self.investigador.nombre, self.numero_cedula,self.nombre_programa)

    class Meta(object):
        verbose_name = "Cédula Investigador"
        verbose_name_plural = "Cédulas Investigador"
        ordering = ['investigador']

