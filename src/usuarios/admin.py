from django.contrib import admin
from .models import Usuario
from .models import CedulaInvestigador
from  .models import InvestigadorNumeroExterna

# Register your models here.
admin.site.register(Usuario)
admin.site.register(CedulaInvestigador)
admin.site.register(InvestigadorNumeroExterna)

