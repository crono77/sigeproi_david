from django.shortcuts import render, redirect
from .forms import EditaPerfilForm, CedulainvForm,PerfilAlumnoAdminForm,NuevaContraseñaExternoForm,NuevaContraseñaAlumnoForm,LoginForm
from django.contrib import messages

# Create your views here.
'''def welcome(request):
    return render(request,"portal/welcome.html")

def registrar(request):
    if request.method == 'POST':
        form =  EditaPerfilForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['Nombre']
            ap_paterno = form.cleaned_data['Ap_Paterno']
            ap_materno = form.cleaned_data['Ap_Materno']
            tipo_usuario = form.cleaned_data['Tipo Usuario']
            curp = form.cleaned_data['Curp']
            rfc = form.cleaned_data['Rfc']
            telefono = form.cleaned_data['Telefono']
            activo = form.cleaned_data['Activo']
            procedencia = form.cleaned_data['Procedencia']
            cargo = form.cleaned_data['Cargo']
            id_carrera = form.cleaned_data['Carrera']
            semestre_actual = form.cleaned_data['Semestre Actual']
            message.success(request,f'Usuario {nombre} creado')
        else:
            form = EditaPerfilForm ()
       
    return render(request,"portal/registrar.html")

def login(request):
    return render(request, "portal/login.html")

def logout(request):
    return redirect('/')
#vista registrar usuario
'''

def registrar_usuario(request):
    if request.method == 'POST':
        form =  EditaPerfilForm(data=request.POST)
        if form.is_valid():
            nombre = request.POST.get('Nombre', False)
            ap_paterno = request.POST.get('Ap_Paterno',False )
            ap_materno = request.POST.get('Ap_Materno',False)
            tipo_usuario = request.POST.get('Tipo Usuario',False)
            curp = request.POST.get('Curp',False)
            rfc = request.POST.get('Rfc',False)
            telefono = request.POST.get('Telefono',False)
            activo = request.POST.get('Activo',False)
            procedencia = request.POST.get('Procedencia',False)
            cargo = request.POST.get('Cargo',False)
            id_carrera = request.POST.get('Carrera',False)
            semestre_actual = request.POST.get('Semestre Actual',False)
            #message.success(request,f'Usuario {nombre} creado')

          #  registrar_expositor=Expositor(nombre=nombre, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, biografia=biografia, email=email
            #  redes_sociales=redes_sociales, nacionalidad=nacionalidad, es_extranjero=es_extranjero, empresa=empresa,
                                         #  puesto_expositor#=puesto_expositor)
           # registrar_expositor.save()
            #messages.add_message(request, messages.INFO, SMS_REGISTRADO)
        #return HttpResponseRedirect('/conferencias/consultar_expositor/')
    else:
        form = EditaPerfilForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Registro Usuario'})
 

#vista Nueva Contraseña Externo

def cedula_investigador(request):
    if request.method == 'POST':
        form = CedulainvForm(request.POST)
        if form.is_valid():
            id_investigador = request.POST['CInvestigador']
            numero_cedula = request.POST['Numero Cedula']
            pais = request.POST['Pais']
            clave_programa = request.POST['Clave_Programa']
            nombre_programa = request.POST['Nombre Programa']
            numero_cedula = request.POST['Numero cedula']
            clave_institucion = request.POST['Clave Institucion']
            nombre_institucion = request.POST['Nombre Institucion']
            fecha_expedicion = request.POST['Fecha Expedicion']


    else:
        form = CedulainvForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Cedula Investigador'})



#vista perfil alumno admin

def PerfilAlumnoAdmin(request):
    if request.method == 'POST':
        form = PerfilAlumnoAdminForm(request.POST)
        if form.is_valid():
            proyectos_involucrados = request.POST['Proyectos Involucrados']
            telefono = request.POST['Telefono']
            encargado = request.POST['Encargado']
            semestre = request.POST['Semestre']
            carrera = request.POST['Carrera']
            personacargo = request.POST['Persona a Cargo']


    else:
        form = PerfilAlumnoAdminForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Perfil alumno admin'})







#vista Nueva Contraseña Externo

def NuevaContraseñaExterno(request):
    if request.method == 'POST':
        form = NuevaContraseñaExternoForm(request.POST)
        if form.is_valid():
            antigua_contrasena = request.POST['Antigua Contraseña']
            nueva_contrasena = request.POST['nueva_contrasena']


    else:
        form = NuevaContraseñaExternoForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Nueva Contraseña Externo'})

#vista nueva contraseña alumno
def NuevaContraseñaAlumno(request):
    if request.method == 'POST':
        form = NuevaContraseñaAlumnoForm(request.POST)
        if form.is_valid():
            antigua_contrasena = request.POST['Antigua Contraseña']
            nueva_contrasena = request.POST['Nueva Contrasena']


    else:
        form = NuevaContraseñaAlumnoForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Nueva Contraseña Alumno'})

# Vista login Usuarios

def Login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = request.POST['Email']
            password = request.POST['Password']
            tipo_usuario = request.POST['Tipo Usuario']



    else:
        form = LoginForm()

    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Nueva Contraseña Alumno'})

