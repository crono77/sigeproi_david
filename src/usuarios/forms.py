from django import forms
from .models import Usuario


class EditaPerfilForm(forms.Form):
    nombre = forms.CharField(label='Nombre', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    ap_paterno = forms.CharField(label='Apellido Paterno', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    ap_materno= forms.CharField(label='Apellido Materno', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo_usuario = forms.CharField(label='Tipo Usuario', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    curp = forms.CharField(label='Curp', required=False, max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    rfc = forms.CharField(label='Rfc', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    telefono = forms.CharField(label='Telefono', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    foto = forms.CharField(label='Foto', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    activo = forms.BooleanField(label='Activo', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    procedencia = forms.CharField(label='Procedencia', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    cargo = forms.CharField(label='Cargo', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_carrera = forms.CharField(label='Carrera', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    semestre_actual  = forms.IntegerField(label='Semestre acutal', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))



class CedulainvForm(forms.Form):
    #id = forms.CharField(label='Nombre', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_investigador = forms.IntegerField(label='Investigador', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    numero_cedula = forms.IntegerField(label='Numero cedula', required=False,   widget=forms.TextInput(attrs={'class': 'form-control'}))
    pais = forms.CharField(label='Pais', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    clave_programa = forms.IntegerField(label='Clave programa', required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    nombre_programa = forms.CharField(label='Nombre programa', required=True, max_length=150,widget=forms.TextInput(attrs={'class': 'form-control'}))
    numero_cedula = forms.CharField(label='Numero cedula', required=False, max_length=250,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    clave_institucion = forms.CharField(label='clave institucion', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    nombre_institucion = forms.IntegerField(label='Nombre institucion', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    fecha_expedicion = forms.DateField(label='Fecha expedicion', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))



class PerfilAlumnoAdminForm(forms.Form):
    proyectos_involucrados = forms.CharField(label='Proyectos Involucrados', required=True, max_length=150,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
    telefono = forms.CharField(label='Telefono', required=True, max_length=150,
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
    encargado = forms.CharField(label='Encargado', required=False, max_length=250,
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
    semestre = forms.IntegerField(label='Semestre', required=False,
                                  widget=forms.TextInput(attrs={'class': 'form-control'}))
    carrera  = forms.CharField(label='Carrera', required=False, max_length=180,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    personacargo = forms.CharField(label='Persona a cargo', required=False, max_length=180,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))


class NuevaContraseñaExternoForm(forms.Form):
        antigua_contrasena = forms.CharField(label='Antigua Contrasena', required=True, max_length=150,
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
        nueva_contrasena = forms.CharField(label='Nueva Contrasena', required=True, max_length=150,
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))


class NuevaContraseñaAlumnoForm(forms.Form):
    antigua_contrasena = forms.CharField(label='Antigua Contrasena', required=True, max_length=150,
                                         widget=forms.TextInput(attrs={'class': 'form-control'}))
    nueva_contrasena = forms.CharField(label='Nueva Contrasena', required=True, max_length=150,
                                       widget=forms.TextInput(attrs={'class': 'form-control'}))


class LoginForm(forms.Form):
    email = forms.CharField(label='Email', required=True, max_length=150,
                                         widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', required=True, max_length=150,
                                       widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo_usuario = forms.CharField(label='Tipo Usuario',required=True, max_length=150,
                                       widget=forms.TextInput(attrs={'class': 'form-control'}))


