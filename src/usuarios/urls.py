"""sigeproi URL Etapas
"""
from django.conf.urls import url,  include
from usuarios import views


urlpatterns = [
    #url(r'^/$',views.welcome,name='form'),
    #url(r'^login/$',views.login,name='form'),
    #url(r'^logout/$',views.logout,name='form'),
    #url(r'^registrar/$',views.registrar,name='form'),
    url(r'^registrar_usuario/$', views.registrar_usuario, name='form'),
    url(r'^cedula_investigador/$', views.cedula_investigador, name='form'),
    url(r'^PerfilAlumnoAdmin/$', views.PerfilAlumnoAdmin, name='form'),
    url(r'^NuevaContraseñaExterno/$', views.NuevaContraseñaExterno, name='form'),
    url(r'^NuevaContraseñaAlumno/$', views.NuevaContraseñaAlumno, name='form'),
    url(r'^Login/$', views.Login, name='form'),
]


