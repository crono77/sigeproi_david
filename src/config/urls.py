"""sigeproi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from portal import views


urlpatterns = [
    url(r'^$', views.inicio, name='inicio'),
    url(r'^form/$', views.form, name='form'),
    url(r'^proyectos/$', views.proyectos, name='proyectos'),
    path('admin/', admin.site.urls),
    path('proyectos/', include('proyectos.urls')),
    path('etapas/', include('etapas.urls')),
    path('usuarios/', include('usuarios.urls'))

    
]


"""
from django.conf.urls import url,include
from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from portal import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.inicio, name='inicio'),
    url(r'^form/$', views.form, name='form'),
    url(r'^proyectos/$', views.proyectos, name='proyectos'),
    #path('proyectos/', include('proyectos.urls')),
    #path('etapas/', include('etapas.urls')),
    #path('usuarios/', include('usuarios.urls'))
]

"""

