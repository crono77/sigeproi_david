"""
entorno.py
Constantes y Utilerias del entorno del sistema
"""

import os
import json

EJECUCION_MODO_PRODUCCION = False

_archivo_secretos = 'secretos.json'  # Ajustar el nombre del archivo de secretos en modo desarrollo

# DIRECTORIO BASE
DIR_BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Archivo de claves y contraseñas
FILE_SECRETOS = os.path.join(DIR_BASE, _archivo_secretos)

# Leer secretos.json
with open(FILE_SECRETOS) as f:
    secretos = json.loads(f.read())


def obtener_secreto(clave, secretos=secretos):
    """ Funcion utileria para obtener las claves secretas"""
    try:
        return secretos[clave]
    except KeyError:
        error_msg = "Error al leer la clave {0} de secretos. Fin".format(clave)
        raise SystemExit(error_msg)

# Configuraciones del SISTEMA
SISTEMA_EMPRESA = {
    'nombre': 'Instituto Tecnológico Superior de Lerdo',
    'email_soporte': 'master@itslerdo.edu.mx'
}

# PAGINADOR_ITEMS_POR_PAGINA
SISTEMA_PAGINADOR_ITEMS_X_PAGINA = 2


#Mensajes Sistema

SMS_ELIMINADO = "Registro Eliminado."
SMS_REGISTRADO = "Nuevo Registro Guardado."
SMS_EDITADO = "Registro Editado."

SMS_ELIMINADO_ERROR = "No fue posible eliminar el registro por que esta asociado a otra entidad."
SMS_ERROR_REGISTRO_ACTIVIDAD = "No fue posible el registro a la actividad porque ya esta registrado"
SMS_ERROR_REGISTRO_AGOTADO="No fue posible el registro a la actividad porque su cupo se ha agotado"

