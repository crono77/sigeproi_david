from django.contrib import admin
from .models import Proyecto
from .models import ProyectoLinea
from .models import ProyectoCarrera
from .models import ProyectoDependeciaExterna
from .models import ProyectoBeneficio
from .models import ProyectoFinanciador
from .models import ProyectoImpacto
from .models import ProyectoIndicador
from .models import ProyectoProducto
from .models import ProductoUsuario
from .models import UsuarioRol
from .models import UsuarioPermiso
from .models import Seguimiento

# Register your models here.
admin.site.register(Proyecto)
admin.site.register(ProyectoLinea)
admin.site.register(ProyectoCarrera)
admin.site.register(ProyectoDependeciaExterna)
admin.site.register(ProyectoFinanciador)
admin.site.register(ProyectoImpacto)
admin.site.register(ProyectoIndicador)
admin.site.register(ProyectoProducto)
admin.site.register(ProductoUsuario)
admin.site.register(UsuarioRol)
admin.site.register(UsuarioPermiso)
admin.site.register(Seguimiento)

