from django import forms

#FORMA ANTEPROYECTO
class AnteproyectoProyectoForm(forms.Form):
    titulo_proyecto = forms.CharField(label='Titulo proyecto', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo_proyecto = forms.CharField(label='Tipo proyecto', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    clave_proyecto = forms.CharField(label='Clave proyecto', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    institucion = forms.CharField(label='Institucion', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    area_institucion = forms.CharField(label='Area institucion', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    prioridad = forms.BooleanField(label='Prioridad', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    fecha_inicio = forms.DateField(label='Fecha_Inicio', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    fecha_final  = forms.DateField(label='Fecha_Final', required=False,  widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(label='Descripcion', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))
    objetivo_general = forms.CharField(label='Objetivo General', required=False, max_length=600, widget=forms.Textarea(attrs={'class': 'form-control'}))
    objetivo_especifico = forms.CharField(label='Objetivo Especifico', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))


class ProyectoForm(forms.Form):
    #lider = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    #tipo_proyecto = models.ForeignKey(TipoProyecto, on_delete=models.PROTECT, null=False)
    #sector = models.ForeignKey(Sector, on_delete=models.PROTECT, null=False)
    titulo_proyecto = forms.CharField(label='Titulo proyecto', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo_proyecto = forms.CharField(label='Tipo proyecto', required=True, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    clave_proyecto = forms.CharField(label='Clave Proyecto', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    institucion = forms.CharField(label='Institucion', required=False, max_length=180, widget=forms.Select(attrs={'class': 'form-control'}))
    area_institucion = forms.CharField(label='Area Institucion', required=False, max_length=180, widget=forms.Select(attrs={'class': 'form-control'}))
    prioridad = forms.BooleanField(label='Prioridad', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    financiamineto_interno = forms.BooleanField(label='Financiamiento Interno', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    financiamineto_externo = forms.BooleanField(label='Financiamiento Interno', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    fecha_inicio = forms.DateField(label='Fecha_Inicio', required=False,  widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    fecha_final  = forms.DateField(label='Fecha_Final', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(label='Descripcion', required=False, max_length=600, widget=forms.Textarea(attrs={'class': 'form-control'}))
    resumen = forms.CharField(label='Resumen', required=False, max_length=600, widget=forms.Textarea(attrs={'class': 'form-control'}))
    introduccion = forms.CharField(label='Introduccion', required=False, max_length=600,  widget=forms.Textarea(attrs={'class': 'form-control'}))
    antecedentes = forms.CharField(label='Antecedentes', required=False, max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))
    marco_teorico = forms.CharField(label='Marco Teorico', required=False, max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))
    objetivo_general = forms.CharField(label='Objetivo General', required=False, max_length=600, widget=forms.Textarea(attrs={'class': 'form-control'}))
    objetivo_especifico = forms.CharField(label='Objetivo Especifico', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))
    metas = forms.CharField(label='Metas', required=False, max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))
    metodologia = forms.CharField(label='Metodologia', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))
    vinculacion_sector_productivo = forms.CharField(label='vinculacion productivo', required=False, max_length=220, widget=forms.TextInput(attrs={'class': 'form-control'}))
    referencias = forms.CharField(label='referencias', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))
    lugar_desarrollo = forms.CharField(label='lugar desarrollo', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    infraestructura = forms.CharField(label='Infraestructura', required=False, max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    resultados = forms.CharField(label='Resultados', required=False, max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    justificacion = forms.CharField(label='Justificacion', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))
    viabilidad = forms.CharField(label='Viabilidad', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    alcances = forms.CharField(label='Alcances', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))
    limitaciones = forms.CharField(label='Limitaciones', required=False, max_length=300, widget=forms.Textarea(attrs={'class': 'form-control'}))
    monto = forms.FloatField(label='Monto', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    estatus = forms.BooleanField(label='Estatus', initial=False, required=False, widget=forms.CheckboxInput(attrs={'class': 'check-personalizado'}))
    #proyecto_padre =models.ForeignKey('self',on_delete=models.PROTECT,null=True,blank=True)


class ConsultaProyectosForm(forms.Form):
    buscar = forms.ChoiceField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    nombre  = forms.CharField(label='Nombre', required=False, max_length=150, widget=forms.Textarea(attrs={'class': 'form-control'}))
    linea_investigacion = forms.CharField(label='Linea Investigacion', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))
    docente  = forms.CharField(label='Docente', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))


#realizar todos los forms que faltan

class ProyectoUsuarioRol(forms.Form):
   #id= forms.IntegerField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
  id_proyecto  = forms.IntegerField(label='Proyecto', required=False, widget=forms.Select(attrs={'class': 'form-control'}))
  id_rol = forms.IntegerField(label='Rol', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
  id_usuario  = forms.IntegerField(label='Usuario', required=False, widget=forms.Select( attrs={'class': 'form-control'}))
  actividad  = forms.CharField(label='Actividad', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
  competencia = forms.CharField(label='Competencia', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))

class ProyectousuarioPermiso(forms.Form):
    #id= forms.IntegerField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_permiso = forms.IntegerField(label='Permiso', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_usuario  = forms.IntegerField(label='Usuario', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    actividad  = forms.CharField(label='Actividad', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    competencia = forms.CharField(label='Competencia', required=False, max_length=250, widget=forms.Textarea(attrs={'class': 'form-control'}))

class ProyectoProyectoSeguimiento(forms.Form):
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    porcentaje_avance = forms.IntegerField(label='Avance', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    fecha_seguimiento  = forms.DateField(label='Fecha_Inicio', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    resultados_impacto  = forms.CharField(label='resultados impacto', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    destinatario_final = forms.CharField(label='destinatario final', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_usuario = forms.IntegerField(label='porcentaje avance ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))

#form proyectos_proyecto_seguimiento
class ProyectoProyectoExterno(forms.Form):
    #id= forms.IntegerField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_externo  = forms.IntegerField(label=' Externo ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    clave   = forms.IntegerField(label='Clave', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

# tbProyectos_Proyecto_Financiador
class Proyectos_Proyecto_Financiador(forms.Form):
    #id= forms.IntegerField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_financiador  = forms.IntegerField(label='Financiador  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    monto   = forms.DecimalField(label='Monto', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion  = forms.CharField(label='Descripcion', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))

# tbProyectos_Financiador
class Proyectos_Financiador(forms.Form):
    nombre_financiador   = forms.IntegerField(label='Nombre Financiador', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    contacto  = forms.IntegerField(label='Contacto  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    telefono   = forms.DecimalField(label='Telefono', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email   = forms.CharField(label='Email', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion   = forms.CharField(label='Descripcion', required=False, max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo   = forms.BooleanField(label='Tipo', required=False, widget=forms.CheckboxInput(attrs={'class': 'form-control'}))
    eliminado   = forms.BooleanField(label='Eliminado', required=False, widget=forms.CheckboxInput(attrs={'class': 'form-control'}))


# tbIndicador Proyecto
class Proyecto_Carrera(forms.Form):
    #id= forms.IntegerField(label='Buscar',  required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_impacto   = forms.IntegerField(label='Impacto  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    descripcion  = forms.CharField(label='Descripcion', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))



# tbProyectos_indicador proyecto
class Proyecto_Indicador_Proyecto(forms.Form):
    id_proyecto  = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_impacto  = forms.IntegerField(label='Impacto  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_indicador  = forms.IntegerField(label='Indicador  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))


# tbProyectos_Producto Proyecto
class Proyecto_Producto_Proyecto(forms.Form):
    nombre_producto  = forms.CharField(label='Nombre Producto ', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    clave_producto  = forms.CharField(label='Clave Producto ', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    titulo_proucto  = forms.CharField(label='Titulo Producto  ', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion  = forms.CharField(label='Descripcion  ', required=False,  widget=forms.Textarea(attrs={'class': 'form-control'}))
    id_proyecto  = forms.IntegerField(label='Numero Proyecto  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_tipo_producto  = forms.IntegerField(label='Tipo Producto ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    fecha_realizacion  = forms.DateField(label='Fecha Realizacion  ', required=False,  widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    ruta  = forms.CharField(label='Ruta  ', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    porcentaje_avance  = forms.IntegerField(label='Porcentaje Avance  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    fecha_seguimiento  = forms.DateField(label='Fecha Seguimiento  ', required=False,  widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    id_usuario  = forms.IntegerField(label='Usuario  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))

# tbProyecto Producto Usuario
class Proyecto_Producto_Usuario(forms.Form):
    id_proyecto  = forms.IntegerField(label='Proyecto ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_usuario  = forms.IntegerField(label='Usuario ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    actividad  = forms.CharField(label='Actividad  ', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    id_rol  = forms.IntegerField(label='Rol  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))

# tbProyectos Proyecto Linea
class Proyecto_Linea(forms.Form):
    id_proyecto  = forms.IntegerField(label='Proyecto ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_linea  = forms.IntegerField(label='Linea ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))

# tbImpacto proyecto
class Impacto_Proyecto(forms.Form):
    id_proyecto  = forms.CharField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_impacto  = forms.CharField(label='Impacto  ', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    Cantidad   = forms.IntegerField(label='Cantidad', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion  = forms.CharField(label='Descripcion', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))


# tbBeneficio proyecto
class Beneficio_Proyecto(forms.Form):
    nombre_beneficio  = forms.CharField(label='Nombre', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    beneficiario_final   = forms.CharField(label='Beneficiario Final', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion  = forms.CharField(label='Descripcion', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))















