from django.shortcuts import render

from .forms import  ConsultaProyectosForm,AnteproyectoProyectoForm,ProyectoUsuarioRol\
    ,ProyectousuarioPermiso,ProyectoProyectoSeguimiento,ProyectoProyectoExterno,ProyectoForm,Proyectos_Proyecto_Financiador,\
    Proyectos_Financiador,Proyecto_Carrera,Proyecto_Indicador_Proyecto,Proyecto_Producto_Proyecto,Proyecto_Producto_Usuario,\
    Proyecto_Linea,Impacto_Proyecto,Beneficio_Proyecto


#vista ANTE PROYECTO

def AnteproyectoProyecto(request):
    if request.method == 'POST':
        form = AnteproyectoProyectoForm(request.POST)
        if form.is_valid():
            titulo_proyecto  = request.POST['Titulo Proyecto']
            tipo_proyecto = request.POST['Tipo Proyecto']
            clave_proyecto = request.POST['Clave Proyecto']
            institucion = request.POST['Institución']
            area_institucion = request.POST['Area Institución']
            prioridad = request.POST['Prioridad']
            fecha_inicio = request.POST['Fecha Inicio']
            fecha_final = request.POST['Fecha Final']
            descripcion = request.POST['Descripcion']
            objetivo_general = request.POST['Objetivo General']
            objetivo_especifico = request.POST['Objetivo Especifico']
    else:
        form = AnteproyectoProyectoForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Consulta Proyectos'})

#vista Consulta Proyectos

def Consulta_Proyectos(request):
    if request.method == 'POST':
        form = ConsultaProyectosForm(request.POST)
        if form.is_valid():
            buscar  = request.POST['Buscar']
            nombre = request.POST['Nombre']
            linea_investigacion = request.POST['Linea Investigacion']
            docente = request.POST['Docente']
    else:
        form = ConsultaProyectosForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Consulta Proyectos'})


# vista ProyectoUsuarioRol
def Usuario_Rol(request):
    if request.method == 'POST':
        form = ProyectoUsuarioRol(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['proyecto']
            id_rol = request.POST['Rol']
            id_usuario = request.POST['Usuario']
            actividad = request.POST['actividad']
            competencia = request.POST['competencia']
    else:
        form = ProyectoUsuarioRol()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Usuario Rol'})


# vista Usuario_Permiso
def Usuario_Permiso(request):
    if request.method == 'POST':
        form = ProyectousuarioPermiso(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_permiso = request.POST['Rol']
            id_usuario = request.POST['Usuario']
            actividad = request.POST['Actividad']
            competencia = request.POST['Competencia']
    else:
        form = ProyectousuarioPermiso()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Usuario Permiso'})

# vista SEGUIMIENTO
def Proyecto_Seguimiento(request):
    if request.method == 'POST':
        form = ProyectoProyectoSeguimiento(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            porcentaje_avance = request.POST['Avance']
            fecha_seguimiento = request.POST['Fecha Seguimiento']
            resultados_impacto = request.POST['Resultados']
            destinatario_final = request.POST['Destinatario Final']
            id_usuario = request.POST['Usuario']
    else:
        form = ProyectoProyectoSeguimiento()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Seguimiento'})

# vista Proyecto externo
def Proyecto_Externo(request):
    if request.method == 'POST':
        form = ProyectoProyectoExterno(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            porcentaje_avance = request.POST['Avance']
            fecha_seguimiento = request.POST['Fecha Seguimiento']
            resultados_impacto = request.POST['Resultados']
            destinatario_final = request.POST['Destinatario Final']
            id_usuario = request.POST['Usuario']
    else:
        form = ProyectoProyectoExterno()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Seguimiento'})

# vista Proyecto FORM
def Proyecto_PROYECTO(request):
    if request.method == 'POST':
        form = ProyectoForm(request.POST)
        if form.is_valid():
            titulo_proyecto  = request.POST['Titulo']
            tipo_proyecto = request.POST['Tipo']
            clave_proyecto = request.POST['Clave']
            institucion = request.POST['Institucion']
            area_institucion = request.POST['Area Institución']
            financiamineto_interno = request.POST['financiamineto_interno']
            financiamineto_externo = request.POST['financiamineto_externo']
            fecha_inicio = request.POST['fecha_inicio']
            fecha_final = request.POST['fecha_final']
            descripcion = request.POST['descripcion']
            resumen = request.POST['resumen']
            introduccion = request.POST['introduccion']
            antecedentes = request.POST['antecedentes']
            marco_teorico = request.POST['marco_teorico']
            objetivo_general = request.POST['objetivo_general']
            objetivo_especifico = request.POST['objetivo_especifico']
            metas = request.POST['metas']
            vinculacion_sector_productivo = request.POST['vinculacion_sector_productivo']
            referencias = request.POST['referencias']
            lugar_desarrollo = request.POST['lugar_desarrollo']
            infraestructura = request.POST['infraestructura']
            resultados = request.POST['resultados']
            justificacion = request.POST['justificacion']
            viabilidad = request.POST['viabilidad']
            alcances = request.POST['alcances']
            limitaciones = request.POST['limitaciones']
            monto = request.POST['monto']
            estatus = request.POST['estatus']
    else:
        form = ProyectoForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Proyeto'})


# vista Proyectos_Proyecto_Financiador
def Proyectos_ProyectoFinanciador(request):
    if request.method == 'POST':
        form = Proyectos_Proyecto_Financiador(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_financiador = request.POST['Financiador']
            monto = request.POST['Monto']
            descripcion = request.POST['Descripcion']
    else:
        form = Proyectos_Proyecto_Financiador()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Proyectos Financiador'})

# vista Proyecto Financiador
def ProyectosFinanciador(request):
    if request.method == 'POST':
        form = Proyectos_Financiador(request.POST)
        if form.is_valid():
            nombre_financiador  = request.POST['Nombre Financiador']
            contacto = request.POST['Contacto']
            telefono = request.POST['Telefono']
            email = request.POST['Email']
            descripcion = request.POST['Descripcion']
            tipo = request.POST['Tipo']
            eliminado = request.POST['Eliminado']
    else:
        form = Proyectos_Financiador()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Financiador'})

# vista Proyecto Carrera
def ProyectoCarrera(request):
    if request.method == 'POST':
        form = Proyecto_Carrera(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Nombre Financiador']
            id_impacto = request.POST['Contacto']
            descripcion = request.POST['Descripcion']
    else:
        form = Proyecto_Carrera()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Carrera'})


# vista Proyecto Indicador
def ProyectoIndicador(request):
    if request.method == 'POST':
        form = Proyecto_Indicador_Proyecto(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_impacto = request.POST['Impacto']
            id_indicador = request.POST['Indicador']
    else:
        form = Proyecto_Indicador_Proyecto()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Indicador'})


# vista Proyecto Producto
def ProyectoProducto(request):
    if request.method == 'POST':
        form = Proyecto_Producto_Proyecto(request.POST)
        if form.is_valid():
            nombre_producto  = request.POST['Nomre']
            clave_producto = request.POST['Clave']
            titulo_proucto = request.POST['Titulo']
            descripcion = request.POST['Descripcion']
            id_proyecto = request.POST['Proyecto']
            id_tipo_producto = request.POST['Tipo Producto']
            fecha_realizacion = request.POST['Fecha Realizacion']
            ruta = request.POST['Ruta']
            porcentaje_avance = request.POST['Porcentaje Avance']
            fecha_seguimiento = request.POST['Fecha Seguimiento']
            id_usuario = request.POST['Usuario']
    else:
        form = Proyecto_Producto_Proyecto()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Producto'})

# vista Proyecto Usuario
def ProyectoProductoUsuario(request):
    if request.method == 'POST':
        form = Proyecto_Producto_Usuario(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_usuario = request.POST['Usuario']
            actividad = request.POST['Actividad']
            id_rol = request.POST['Rol']
    else:
        form = Proyecto_Producto_Usuario()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Producto Usuario'})


# vista Proyecto Linea
def ProyectoLinea(request):
    if request.method == 'POST':
        form = Proyecto_Linea(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_linea = request.POST['Linea']
    else:
        form = Proyecto_Linea()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Linea'})


# vista Proyecto Impacto
def ProyectoImpacto(request):
    if request.method == 'POST':
        form = Impacto_Proyecto(request.POST)
        if form.is_valid():
            id_proyecto  = request.POST['Proyecto']
            id_impacto = request.POST['Impacto']
            Cantidad = request.POST['Cantidad']
            descripcion = request.POST['Descripcion']
    else:
        form = Impacto_Proyecto()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Impacto'})



# vista Proyecto Beneficio
def ProyectoBeneficio(request):
    if request.method == 'POST':
        form = Beneficio_Proyecto(request.POST)
        if form.is_valid():
            nombre_beneficio  = request.POST['Nombre']
            beneficiario_final = request.POST['Beneficiario Final']
            descripcion = request.POST['Descripcion']
    else:
        form = Beneficio_Proyecto()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Beneficio'})














