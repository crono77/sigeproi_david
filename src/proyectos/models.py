from django.db import models
from catalogos.models import Impacto
from catalogos.models import Indicador
from catalogos.models import Lineas
from catalogos.models import Carrera
from usuarios.models import Usuario
from catalogos.models import TipoProyecto
from catalogos.models import Sector
from catalogos.models import Rol
from catalogos.models import Permisos
from catalogos.models import TipoProducto
from catalogos.models import Financiador
from catalogos.models import DependenciaExterna


class Proyecto(models.Model):
    estatus = (
        (1, 'Autorizado'),
        (2, 'Nuevo'),
        (3, 'En Proceso'),
        (4, 'Finalizado '),
        (5, 'Cancelado'),
        (6, 'Detenido'),
    )
    prioridad =(
        (1, 'Sin prioridad'),
        (2, 'Baja'),
        (3, 'Media'),
        (4, 'Alta '),
    )
    lider = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    tipo_proyecto = models.ForeignKey(TipoProyecto, on_delete=models.PROTECT, null=False)
    sector = models.ForeignKey(Sector, on_delete=models.PROTECT, null=False)
    clave_proyecto = models.CharField(max_length=50, null=False, blank=False)
    titulo_proyecto = models.TextField(null=False, blank=False)
    institucion = models.CharField(max_length=255, null=False, blank=False)
    area_institucion = models.TextField(null=False, blank=False)
    prioridad = models.IntegerField(choices=prioridad)
    financiamineto_interno = models.IntegerField()
    financiamineto_externo = models.IntegerField()
    fecha_inicio = models.DateField(null=False, blank=False)
    fecha_final = models.DateField(null=False, blank=False)
    descripcion = models.TextField(null=False, blank=False)
    resumen = models.TextField(null=True, blank=True)
    introduccion = models.TextField(null=False, blank=False)
    antecedentes = models.TextField(null=True, blank=True)
    marco_teorico = models.TextField(null=True, blank=True)
    objetivo_general = models.CharField(max_length=255, null=False, blank=False)
    objetivo_especifico = models.TextField(null=False, blank=False)
    metas = models.TextField(null=True, blank=True)
    metodologia = models.TextField(null=True, blank=True)
    vinculacion_sector_productivo = models.TextField(null=True, blank=True)
    referencias = models.TextField(null=True, blank=True)
    lugar_desarrollo = models.TextField(null=True, blank=True)
    infraestructura = models.TextField(null=True, blank=True)
    resultados = models.TextField(null=True, blank=True)
    justificacion = models.TextField(null=True, blank=True)
    viabilidad = models.TextField(null=True, blank=True)
    alcances = models.TextField(null=True, blank=True)
    limitaciones = models.TextField(null=True, blank=True)
    monto = models.FloatField(null=True, blank=True)
    estatus = models.IntegerField(choices=estatus)
    proyecto_padre =models.ForeignKey('self',on_delete=models.PROTECT,null=True,blank=True)
    def __str__(self):
        return "%s - %s - %s" % (self.clave_proyecto, self.titulo_proyecto, self.lider.nombre)

    class Meta(object):
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyecto"
        ordering = ['clave_proyecto']


class ProyectoIndicador(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    indicador = models.ForeignKey(Indicador, on_delete=models.PROTECT, null=False)
    descripcion = models.TextField(null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.proyecto.titulo_proyecto,self.indicador.descripcion)

    class Meta(object):
        verbose_name = "Indicador Proyecto"
        verbose_name_plural = "Indicador Proyecto"
        ordering = ['proyecto']


class ProyectoLinea(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    linea = models.ForeignKey(Lineas, on_delete=models.PROTECT, null=False)

    def __str__(self):
        return "%s - %s" % (self.proyecto.titulo_proyecto, self.linea.nombre_linea)

    class Meta(object):
        verbose_name = "Proyecto Linea"
        verbose_name_plural = "Proyecto Linea"
        ordering = ['proyecto']


class ProyectoCarrera(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    carrera = models.ForeignKey(Carrera, on_delete=models.PROTECT, null=False)

 #   def __str__(self):
  #      return "%s - %s - %s" % (self.Proyecto.titulo_proyecto, self.Carrera.nombre_largo)

    class Meta(object):
        verbose_name = "Proyecto Carrera"
        verbose_name_plural = "Proyectos Carrera"
        ordering = ['proyecto']


class ProyectoProducto(models.Model):
    nombre_producto = models.CharField(max_length=50, null=False, blank=False)
    titulo_producto = models.CharField(max_length=50, null=False, blank=False)
    descripcion = models.TextField(null=False, blank=False)
    tipo_producto = models.ForeignKey(TipoProducto, on_delete=models.PROTECT, null=False)
    proyecto  = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    fecha_realizacion =  models.CharField(max_length=50, null=False, blank=False)
    ruta = models.TextField(null=False, blank=False)
    porcentaje_avance = models.IntegerField(null=False, blank=False)
    fecha_seguimiento = models.DateTimeField(null=False, blank=False)
    usuario =  models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)

    def __str__(self):
        return "%s - %s - %s - %s " % (self.nombre_producto, self.titulo_producto,
                                      self.proyecto.titulo_proyecto,self.tipo_producto.descripcion)

    class Meta(object):
        verbose_name = "Producto del Proyecto"
        verbose_name_plural = "Productos del Proyecto"
        ordering = ['nombre_producto']


class ProductoUsuario (models.Model):
    producto = models.ForeignKey(ProyectoProducto, on_delete=models.PROTECT, null=False)
    usuario  = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    actividad = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s - %s - %s " % (self.producto.nombre_producto,self.usuario.nombre,self.producto.proyecto.titulo_proyecto)

    class Meta(object):
        verbose_name = "Producto Usuario"
        verbose_name_plural = "Productos Usuario "
        ordering = ['producto']



class Seguimiento(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    porcentaje_avance = models.IntegerField(null=False, blank=False)
    fecha_seguimiento = models.DateTimeField(null=False, blank=False)
    resultado_impacto = models.TextField()
    destinatario_final = models.CharField(max_length=250, null=False, blank=False)

 #   def __str__(self):
#        return "%s - %s - %s" % (self.proyecto.titulo_proyecto, self.Porcentaje_Avance,
                                        #   self.fecha_seguimiento)

    class Meta(object):
        verbose_name = "Seguimiento"
        verbose_name_plural = "Seguimientos"
        ordering = ['proyecto']


class UsuarioRol(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    rol = models.ForeignKey(Rol, on_delete=models.PROTECT, null=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    actividad = models.TextField()

    def __str__(self):
        return "%s - %s - %s" % (self.usuario.nombre,self.rol.nombre_rol,self.proyecto.titulo_proyecto)

    class Meta(object):
        verbose_name = "Rol del Usuario"
        verbose_name_plural = "Roles del Usuario"
        ordering = ['proyecto']


class UsuarioPermiso(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    permiso = models.ForeignKey(Permisos, on_delete=models.PROTECT, null=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    actividad = models.TextField()

    def __str__(self):
        return "%s - %s - %s" % (self.usuario.nombre,self.permiso.nombre_permiso,self.proyecto.titulo_proyecto)

    class Meta(object):
        verbose_name = "Permiso del Usuario"
        verbose_name_plural = "Permisos del Usuario"
        ordering = ['proyecto']



class ProyectoImpacto(models.Model):
    impacto = models.ForeignKey(Impacto, on_delete=models.PROTECT, null=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    descripcion = models.CharField(max_length=50, null=False, blank=False)
    cantidad = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.proyecto.titulo_proyecto,self.impacto.descripcion)

    class Meta(object):
        verbose_name = "Impacto Proyecto"
        verbose_name_plural = "Impacto Proyecto"
        ordering = ['proyecto']


class ProyectoFinanciador(models.Model):
    proyecto  = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    financiador  = models.ForeignKey(Financiador, on_delete=models.PROTECT, null=False)
    monto = models.IntegerField(null=False, blank=False)
    descripcion = models.TextField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.proyecto.titulo_proyecto,self.financiador.nombre_financiador)

    class Meta(object):
        verbose_name = "Proyecto Financiador"
        verbose_name_plural = "Proyecto Financiador"
        ordering = ['proyecto']


class ProyectoBeneficio(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    nombre_beneficio = models.CharField(max_length=50, null=False, blank=False)
    descripcion  = models.CharField(max_length=50, null=False, blank=False)
    beneficiario_final = models.TextField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.Proyecto.titulo_proyecto,self.Nombre_Beneficio)

    class Meta(object):
        verbose_name = "Beneficio Proyecto "
        verbose_name_plural = "Beneficio Proyecto"
        ordering = ['proyecto']


class ProyectoDependeciaExterna(models.Model):
    proyecto  = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    dependencia  = models.ForeignKey(DependenciaExterna, on_delete=models.PROTECT, null=False)
    clave = models.CharField(max_length=30, null=False, blank=False)

 #   def __str__(self):
#        return "%s - %s - %s " % (self.Proyecto.titulo_proyecto,self.externo.nombre_largo,self.clave)

    class Meta(object):
        verbose_name = "Dependencia Externa del Proyecto"
        verbose_name_plural = "Dependencias Externas del Proyecto"
        ordering = ['proyecto']
