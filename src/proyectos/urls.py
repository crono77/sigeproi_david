"""sigeproi URL proyectos
"""
from django.conf.urls import url, include
from proyectos import views

urlpatterns = [
    url(r'^Consulta_Proyectos/$', views.Consulta_Proyectos, name='form'),
    url(r'^AnteproyectoProyecto/$', views.AnteproyectoProyecto, name='form'),
    url(r'^Usuario_Rol/$', views.Usuario_Rol, name='form'),
    url(r'^Usuario_Permiso/$', views.Usuario_Permiso, name='form'),
    url(r'^Proyecto_Seguimiento/$', views.Proyecto_Seguimiento, name='form'),
    url(r'^Proyecto_Externo/$', views.Proyecto_Externo, name='form'),
    url(r'^Proyecto_PROYECTO/$', views.Proyecto_PROYECTO, name='form'),
    url(r'^Proyectos_ProyectoFinanciador/$', views.Proyectos_ProyectoFinanciador, name='form'),
    url(r'^ProyectosFinanciador/$', views.ProyectosFinanciador, name='form'),
    url(r'^ProyectoCarrera/$', views.ProyectoCarrera, name='form'),
    url(r'^ProyectoIndicador/$', views.ProyectoIndicador, name='form'),
    url(r'^ProyectoProducto/$', views.ProyectoProducto, name='form'),
    url(r'^ProyectoProductoUsuario/$', views.ProyectoProductoUsuario, name='form'),

    url(r'^ProyectoLinea/$', views.ProyectoLinea, name='form'),
    url(r'^ProyectoImpacto/$', views.ProyectoImpacto, name='form'),
    url(r'^ProyectoBeneficio/$', views.ProyectoBeneficio, name='form'),
]



