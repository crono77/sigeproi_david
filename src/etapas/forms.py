from django import forms
from .models import Etapa,CapituloPartida

class etapasForm(forms.Form):
    clave_etapa = forms.CharField(label='clave etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    nombre_etapa = forms.CharField(label='nombre etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    monto_etapa = forms.FloatField(label='Monto', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    fecha_inicio  = forms.DateField(label='fecha inicio', required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    fecha_final  = forms.DateField(label='fecha final', required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    porcentaje_avance = forms.CharField(label='clave etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(label='nombre etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    meta = forms.CharField(label='meta', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    actividad = forms.CharField(label='Actividad', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    producto = forms.CharField(label='Producto', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    estatus = forms.CharField(label='clave etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    prioridad = forms.CharField(label='nombre etapa', required=False, max_length=150, widget=forms.TextInput(attrs={'class': 'form-control'}))
    ultima_etapa = forms.FloatField(label='ultima etapa', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    #proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    #etapa_padre = models.ForeignKey('self',on_delete=models.PROTECT,null=True,blank=True)
   # lider = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)

#ETAPA GASTO
class gastoForm(forms.Form):
    id_etapa = forms.ModelChoiceField(label='*Etapa',
                                              widget=forms.Select(attrs={'class': 'form-control'}),
                                              queryset=CapituloPartida.objects.all())

    id_partida_gasto = forms.ModelChoiceField(label='*Partida gasto',
                                          widget=forms.Select(attrs={'class': 'form-control'}),
                                          queryset=Etapa.objects.all())

    numero_gasto = forms.IntegerField(label='Numero gasto', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    nombre_gasto = forms.CharField(label='Nombre gasto', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    aportacion = forms.CharField(label='Aportacion', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    rubro = forms.IntegerField(label='Rubro', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    justificacion = forms.CharField(label='Justificacion', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))
    importe = forms.DecimalField(label='Importe', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

#ETAPA CARRERA
class carreraForm(forms.Form):
    #id = forms.IntegerField(label='id etapa', required=False, max_length=150, widget=forms.Textarea(attrs={'class': 'form-control'}))
    id_etapa = forms.IntegerField(label='Etapa', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_carrera = forms.IntegerField(label='Carrera', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(label='Descripcion', required=False, max_length=180, widget=forms.Textarea(attrs={'class': 'form-control'}))

#ETAPA LINEAS
class lineasForm(forms.Form):
    #id = forms.IntegerField(label='id etapa', required=False, max_length=150, widget=flsorms.Textarea(attrs={'class': 'form-control'}))
    id_etapa = forms.IntegerField(label='Etapa', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_linea = forms.IntegerField(label='Linea', required=False, widget=forms.Select(attrs={'class': 'form-control'}))

#ETAPA AJUSTES
class ajustesForm(forms.Form):
    #id = forms.IntegerField(label='id etapa', required=False, max_length=150, widget=forms.Textarea(attrs={'class': 'form-control'}))
    id_usuario = forms.IntegerField(label='Usuario', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_proyecto = forms.IntegerField(label='Proyecto', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    id_etapa = forms.IntegerField(label='Etapa', required=False,  widget=forms.Select(attrs={'class': 'form-control'}))
    fecha_termino_anterior = forms.DateField(label='Fecha_Inicio', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    fecha_termino_nueva = forms.DateField(label='Fecha_Inicio', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    motivo = forms.CharField(label='Motivo', required=False, max_length=180, widget=forms.TextInput(attrs={'class': 'form-control'}))
    fecha_modificacion = forms.DateField(label='Fecha_Inicio', required=False, widget=forms.SelectDateWidget(attrs={'class': 'form-control'}))
    estatus = forms.CharField(label='Estatus', required=False,  widget=forms.TextInput(attrs={'class': 'form-control'}))



