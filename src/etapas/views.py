from django.contrib import messages
from django.shortcuts import render
from .forms import  gastoForm,etapasForm,carreraForm,lineasForm,ajustesForm
from .models import Etapa,Gasto,CapituloPartida

from config.entorno import *

#vista Etapas_etapa
def etapasForm(request):
    if request.method == 'POST':
        form = etapasForm(request.POST)
        if form.is_valid():
            clave_etapa  = request.POST['Clave etapa']
            nombre_etapa = request.POST['Nombre etapa']
            monto_etapa = request.POST['Monto etapa']
            fecha_inicio = request.POST['Fecha inicio']
            fecha_final = request.POST['Fecha_final']
            porcentaje_avance = request.POST['Porcentaje avance']
            descripcion = request.POST['Descripcion']
            meta = request.POST['Meta']
            actividad = request.POST['Actividad']
            producto = request.POST['Producto']
            estatus = request.POST['Estatus']
            prioridad = request.POST['Prioridad']
            ultima_etapa = request.POST['Ultima Etapa']
    else:
        form = etapasForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'etapasForm'})


#vista gasto form

def GastoForm(request):
    if request.method == 'POST':
        form = gastoForm(request.POST)
        if form.is_valid():
            etapa  = request.POST['id_etapa']
            partida_gasto = request.POST['id_partida_gasto']
            numero_gasto = request.POST['numero_gasto']
            nombre_gasto = request.POST['nombre_gasto']
            aportacion = request.POST['aportacion']
            rubro = request.POST['rubro']
            justificacion = request.POST['justificacion']
            importe = request.POST['importe']

            print(etapa)

            #ForeingKey
            etapaetapa_id = Etapa.objects.get(id=etapa)
            partida_gasto_id = CapituloPartida.objects.get(id=partida_gasto)


            objGasto = Gasto(etapa=etapaetapa_id,partida_gasto=partida_gasto_id,
                             numero_gasto=numero_gasto,nombre_gasto=nombre_gasto,
                             aportacion=aportacion,rubro=rubro,
                             justificacion=justificacion,
                             importe=importe)
            objGasto.save()
            messages.add_message(request, messages.INFO, SMS_REGISTRADO)
    else:
        form = gastoForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Gastos'})

#vista carrera Form
def Carrera(request):
    if request.method == 'POST':
        form = carreraForm(request.POST)
        if form.is_valid():
            id_etapa  = request.POST['Etapa']
            id_carrera = request.POST['Carrera']
            descripcion = request.POST['Descripcion']
    else:
        form = carreraForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Carrera'})
#vista linea
def linea(request):
    if request.method == 'POST':
        form = lineasForm(request.POST)
        if form.is_valid():
            id_etapa  = request.POST['id_etapa']
            id_linea = request.POST['id_linea']
    else:
        form = lineasForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Carrera'})

#vista Ajustes
def Ajustes(request):
    if request.method == 'POST':
        form = ajustesForm(request.POST)
        if form.is_valid():
            id_usuario  = request.POST['Usuario']
            id_proyecto = request.POST['Proyecto']
            id_etapa = request.POST['Etapa']
            fecha_termino_anterior = request.POST['fecha_termino_anterior']
            fecha_termino_nueva = request.POST['fecha_termino_nueva']
            motivo = request.POST['Motivo']
            fecha_modificacion = request.POST['Fecha_modificacion']
            estatus = request.POST['Estatus']
    else:
        form = ajustesForm()
    return render(request, 'portal/form.html', {'form': form, 'tituloform': 'Ajustes'})





