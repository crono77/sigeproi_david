from django.contrib import admin

from .models import Etapa
from .models import EtapaCarrera
from .models import EtapasLineas
from .models import Gasto
from .models import Ajustes

# Register your models here.
admin.site.register(Etapa)
admin.site.register(EtapaCarrera)
admin.site.register(EtapasLineas)
admin.site.register(Gasto)
admin.site.register(Ajustes)