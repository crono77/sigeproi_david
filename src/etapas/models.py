
from django.db import models
from catalogos.models import Carrera
from catalogos.models import Lineas
from proyectos.models import Proyecto
from usuarios.models import Usuario
from catalogos.models import CapituloPartida


class Etapa(models.Model):
    estatus = (
        (1, 'Nuevo'),
        (2, 'En Proceso'),
        (3, 'Finalizado '),
        (4, 'Cancelado'),
        (5, 'Detenido'),
    )
    prioridad = (
        (1, 'Sin prioridad'),
        (2, 'Baja'),
        (3, 'Media'),
        (4, 'Alta '),
    )
    clave_etapa = models.CharField(max_length=50, null=False, blank=False)
    nombre_etapa = models.CharField(max_length=50, null=False, blank=False)
    monto_etapa = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_inicio = models.DateField(auto_now=True, null=False, blank=False)
    fecha_final = models.DateField(auto_now=True, null=False, blank=False)
    porcentaje_avance = models.IntegerField()
    descripcion = models.TextField()
    meta = models.TextField()
    actividad = models.TextField()
    producto = models.TextField()
    estatus = models.IntegerField(choices=estatus)
    prioridad = models.IntegerField(choices=prioridad)
    ultima_etapa = models.BooleanField(null=False, blank=False,default=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    etapa_padre = models.ForeignKey('self',on_delete=models.PROTECT,null=True,blank=True)
    lider = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)

    def __str__(self):
        return "%s - %s - %s -%s " % (self.clave_etapa, self.nombre_etapa, self.estatus,self.proyecto.clave_proyecto)

    class Meta(object):
        verbose_name = "Etapa"
        verbose_name_plural = "Etapas"
        ordering = ['clave_etapa']



class Gasto(models.Model):
    etapa=models.ForeignKey(Etapa, on_delete=models.PROTECT, null=False)
    partida_gasto = models.ForeignKey(CapituloPartida, on_delete=models.PROTECT, null=False)
    numero_gasto = models.IntegerField(null=False, blank=False)
    nombre_gasto = models.CharField(max_length=50, null=False, blank=False)
    aportacion = models.CharField(max_length=50, null=False, blank=False)
    rubro = models.CharField(max_length=25, null=False, blank=False)
    justificacion =models.TextField()
    importe = models.DecimalField(max_digits=10, decimal_places=2)


    def __str__(self):
        return "%s - %s - %s -%s " % (self.numero_gasto, self.nombre_gasto,
                                  self.partida_gasto.nombre_partida,self.etapa.clave_etapa )

    class Meta(object):
        verbose_name = "Gasto"
        verbose_name_plural = "Gastos"
        ordering = ['numero_gasto']



class EtapaCarrera(models.Model):
    etapa= models.ForeignKey(Etapa, on_delete=models.PROTECT, null=False)
    carrera= models.ForeignKey(Carrera, on_delete=models.PROTECT, null=False)
    descripcion=models.TextField;


    def __str__(self):
        return "%s - %s " % (self.etapa.nombre_etapa, self.carrera.nombre_largo)

    class Meta(object):
        verbose_name = "División de Etapa"
        verbose_name_plural = "Divisiones de Etapa"
        ordering = ['etapa']



class EtapasLineas(models.Model):
    etapa = models.ForeignKey(Etapa, on_delete=models.PROTECT, null=False)
    linea = models.ForeignKey(Lineas, on_delete=models.PROTECT, null=False)


    def __str__(self):
        return "%s - %s " % (self.etapa.nombre_etapa, self.linea.nombre_linea)

    class Meta(object):
        verbose_name = "Línea de Investigación de Etapa"
        verbose_name_plural = "Líneas de Investigación de Etapa"
        ordering = ['etapa']



class Ajustes(models.Model):
    estatus = (
        (1, 'En Proceso'),
        (2, 'Finalizado '),
        (3, 'Cancelado'),
        (4, 'Detenido'),

    )
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT, null=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    etapa = models.ForeignKey(Etapa, on_delete=models.PROTECT, null=False)
    fecha_termino_anterior = models.DateField(auto_now=False, null=False, blank=False)
    fecha_termino_nueva = models.DateField(auto_now=True, null=False, blank=False)
    motivo = models.TextField(null=False)
    fecha_modificacion = models.DateField(auto_now=True, null=False, blank=False)
    estatus = models.IntegerField(choices=estatus)

    def __str__(self):
        return "%s - %s - %s - %s" % (self.proyecto.titulo_proyecto, self.etapa.nombre_etapa,
                                self.usuario.nombre, self.fecha_modificacion)

    class Meta(object):
        verbose_name = "Ajuste"
        verbose_name_plural = "Ajustes"
        ordering = ['fecha_modificacion']

