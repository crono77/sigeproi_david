from django.shortcuts import render


def inicio(request):
    return render(request,'portal/contenido.html')

def form(request):
    return render(request,'portal/form.html')

def proyectos(request):
    return render(request,'portal/proyectos.html')

