from django.db import models


class Capitulo(models.Model):
    numero_capitulo = models.CharField(max_length=20, null=False,blank=False)
    nombre_capitulo = models.CharField(max_length=100,null=False,blank=False)
    capitulo_padre = models.ForeignKey('self',on_delete=models.PROTECT,null=True,blank=True) #no es requerido

    def __str__(self):
           return "%s - %s " %(self.numero_capitulo,self.nombre_capitulo)

    class Meta(object):
         verbose_name="Capitulo"
         verbose_name_plural="Capitulos"
         ordering=['numero_capitulo']


class CapituloPartida(models.Model):
    tipo_partida = (
        (1, 'Génerica'),
        (2, 'Específica'),
           )
    numero_partida = models.CharField(max_length=20, null=False, blank=False)
    nombre_partida = models.CharField(max_length=100, null=False, blank=False)
    tipo_partida = models.IntegerField(choices=tipo_partida)
    capitulo = models.ForeignKey(Capitulo, on_delete=models.PROTECT, null=False)

    def __str__(self):
           return "%s - %s - %s " %(self.numero_partida,self.nombre_partida,self.capitulo.nombre_capitulo)

    class Meta(object):
         verbose_name="Partida"
         verbose_name_plural="Partidas"
         ordering=['numero_partida']

class Rol(models.Model):
    nombre_rol = models.CharField(max_length=25)

    def __str__(self):
        return "%s" %(self.nombre_rol)

    class Meta(object):
        verbose_name="Rol"
        verbose_name_plural = "Roles"


class Carrera(models.Model):
    nombre_corto = models.CharField(max_length=50, null=False, blank=False)
    nombre_largo = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return "%s - %s" %(self.nombre_corto,self.nombre_largo)

    class Meta(object):
        verbose_name="Catalogo Carrera"
        verbose_name_plural="Catalogos Carreras"
        ordering=['nombre_corto']


class Impacto(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return "%s" %(self.descripcion)

    class Meta(object):
        verbose_name="Impacto"
        verbose_name_plural ="Impactos"


class DependenciaExterna(models.Model):
    nombre_corto = models.CharField(max_length=50, null=False, blank=False)
    nombre_largo = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return "%s - %s" %(self.nombre_corto,self.nombre_largo)

    class Meta(object):
        verbose_name="Dependencia Externa"
        verbose_name_plural="Dependencias Externa"
        ordering=['nombre_corto']


class Indicador(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    class Meta(object):
        verbose_name="Indicador"
        verbose_name_plural = "Indicadores"


class Lineas(models.Model):
    carrera = models.ForeignKey(Carrera, on_delete=models.PROTECT, null=False)
    nombre_linea = models.CharField(max_length=50, null=False, blank=False)
    clave_linea = models.CharField(max_length=30, null=False, blank=False)
    grado_linea = models.CharField(max_length=25, null=False, blank=False)
    temas_prioritarios = models.CharField(max_length=50, null=False, blank=False)
    justificacion = models.CharField(max_length=50, null=False, blank=False)
    tecnm = models.IntegerField(null=False, blank=False)
    prodep = models.IntegerField(null=False, blank=False)
    fecha_alta = models.DateField(null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.nombre_linea , self.carrera.nombre_largo)

    class Meta(object):
        verbose_name = "Linea"
        verbose_name_plural = "Lineas"
        ordering = ['nombre_linea']


class ConsecutivoProyecto(models.Model):
    carrera = models.ForeignKey(Carrera, on_delete=models.PROTECT, null=False)
    consecutivo_proyecto = models.IntegerField(null=False, blank=False)
    anio = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return "%s - %s" % (self.carrera.nombre_largo, self.consecutivo_proyecto)

    class Meta(object):
        verbose_name = "Consecutivo Proyecto"
        verbose_name_plural = "Consecutivo Proyectos"
        ordering = ['consecutivo_proyecto']


class Permisos(models.Model):
    nombre_permiso = models.CharField(max_length=50, null=False, blank=False)
    descripcion = models.CharField(max_length=50, null=False, blank=False)
    es_proyecto = models.BooleanField(default=False, blank=False)
    es_etapa = models.BooleanField(default=False, null= False, blank=False )
    es_gasto = models.BooleanField(default=False, blank=False)

    def __str__(self):
        return "%s - %s - %s - %s  -%s" % (self.nombre_permiso, self.descripcion, self.es_proyecto,self.es_etapa, self.es_gasto)

    class Meta(object):
        verbose_name = "Permiso"
        verbose_name_plural = "Permisos"
        ordering = ['nombre_permiso']


class PermisosRol(models.Model):
    permiso  = models.ForeignKey(Permisos, on_delete=models.PROTECT, null=False)
    rol = models.ForeignKey(Rol, on_delete=models.PROTECT, null=False)

    def __str__(self):
        return "%s - %s" % (self.permiso.nombre_permiso , self.rol.nombre_rol)

    class Meta(object):
        verbose_name = "Permiso Rol"
        verbose_name_plural = "Permisos Rol"
        ordering = ['rol']


class TipoProyecto(models.Model):
    nombre_tipo_proyecto = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s " % (self.nombre_tipo_proyecto)

    class Meta(object):
        verbose_name = "Tipo Proyecto"
        verbose_name_plural = "Tipos Proyectos"
        ordering = ['nombre_tipo_proyecto']


class Sector(models.Model):
    nombre_sector= models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s" % (self.nombre_sector)

    class Meta(object):
        verbose_name = "Sector"
        verbose_name_plural = "Sectores"
        ordering = ['nombre_sector']


class TipoProducto(models.Model):
    descripcion  = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s" % (self.descripcion)

    class Meta(object):
        verbose_name = "Tipo Producto"
        verbose_name_plural = "Tipos Productos"


class Financiador(models.Model):
    nombre_financiador = models.CharField(max_length=50, null=False, blank=False)
    contacto = models.CharField(max_length=50, null=False, blank=False)
    telefono = models.IntegerField(null=False, blank=False)
    email = models.CharField(max_length=50, null=False, blank=False)
    descripcion = models.CharField(max_length=50, null=False, blank=False)
    tipo = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "%s - %s " % (self.nombre_financiador, self.tipo)

    class Meta(object):
        verbose_name = "Proyectos Financiador"
        verbose_name_plural = "Proyectos Financiador "
        ordering = ['nombre_financiador']












