from django.contrib import admin
from .models import Capitulo
from .models import CapituloPartida
from .models import Rol
from .models import Carrera
from .models import Impacto
from .models import DependenciaExterna
from .models import Indicador
from .models import Lineas
from .models import ConsecutivoProyecto
from .models import PermisosRol
from .models import TipoProyecto
from .models import Sector
from .models import TipoProducto
from .models import Permisos
from .models import Financiador


class CapituloAdmin(admin.ModelAdmin):
    list_display = ('numero_capitulo','nombre_capitulo',
                    'capitulo_padre')
    list_display_links = ('numero_capitulo','nombre_capitulo',)
    search_fields = ['numero_capitulo','nombre_capitulo']


class CapituloPartidaAdmin(admin.ModelAdmin):
    list_display = ('numero_partida','nombre_partida',
                    'tipo_partida','capitulo')
    list_display_links = ('numero_partida','nombre_partida',)
    search_fields = ['numero_partida','numero_partida']

class RolAdmin(admin.ModelAdmin):
    list_display = ('nombre_rol',)
    list_display_links = ('nombre_rol',)
    search_fields = ['nombre_rol',]

class CarreraAdmin(admin.ModelAdmin):
    list_display = ('nombre_corto','nombre_largo')
    list_display_links = ('nombre_corto','nombre_largo',)
    search_fields = ['nombre_corto','nombre_largo']

class ImpactoAdmin(admin.ModelAdmin):
    list_display = ('descripcion',)
    list_display_links = ('descripcion',)
    search_fields = ['numero_partida',]

class DependenciaExternaAdmin(admin.ModelAdmin):
    list_display = ('nombre_corto','nombre_largo')
    list_display_links = ('nombre_corto','nombre_largo',)
    search_fields = ['nombre_corto','nombre_largo']

class IndicadorAdmin(admin.ModelAdmin):
    list_display = ('descripcion',)
    list_display_links = ('descripcion',)
    search_fields = ['descripcion',]

class LineasAdmin(admin.ModelAdmin):
    list_display = ('numero_partida','nombre_partida',
                    'tipo_partida','capitulo')
    list_display_links = ('numero_partida','nombre_partida',)
    search_fields = ['numero_partida','numero_partida']



admin.site.register(Capitulo,CapituloAdmin)
admin.site.register(CapituloPartida)
admin.site.register(Rol)
admin.site.register(Carrera)
admin.site.register(Impacto)
admin.site.register(DependenciaExterna)
admin.site.register(Indicador)
admin.site.register(Lineas)
admin.site.register(ConsecutivoProyecto)
admin.site.register(PermisosRol)
admin.site.register(TipoProyecto)
admin.site.register(Sector)
admin.site.register(TipoProducto)
admin.site.register(Permisos)
admin.site.register(Financiador)